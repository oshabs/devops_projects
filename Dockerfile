FROM node:19-alpine3.16 AS builder

# Set the working directory to /app inside the container
WORKDIR /app

COPY package.json package.json
COPY package-lock.json package-lock.json

# Copy app files

COPY public/ public
COPY src/ src

# Install dependencies (npm ci makes sure the exact versions in the lockfile gets installed)
RUN npm ci

# Expose react to port 3000
# EXPOSE 3000

# Start the application
# CMD ["npm", "start"]

# Build the app
 RUN npm run build

# Bundle static assets with nginx
 FROM nginx:1.23.4-alpine as production
 ENV NODE_ENV production

# Copy built assets from `builder` image
 COPY --from=builder /app/build /usr/share/nginx/html

# Add your nginx.conf
 COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose port
 EXPOSE 80

# Start nginx
 CMD ["nginx", "-g", "daemon off;"]

 HEALTHCHECK --interval=5s CMD ping -c 1 172.17.0.2